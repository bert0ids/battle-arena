﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPlayer : Player
{
    public Player HumanPlayer;

    public IEnumerator PickRandomMove()
    {
        yield return new WaitUntil(() => HumanPlayer.Decided);
        base.CurrentChampion.Target = HumanPlayer.CurrentChampion;
        base.PickedAction = base.CurrentChampion.actions[Random.Range(0, base.CurrentChampion.actions.Count)];
        base.Decided = true;
    }
}
