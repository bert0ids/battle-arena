﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Champion : MonoBehaviour
{
    public string Name;
    public Champion Target;
    private Stats stat;
    public Stats ChampStat { get { return stat; } }
    // public List<Operation> Skills = new List<Operation>();
    public List<Actions> actions = new List<Actions>();
    private void Awake()
    {
        for (int i = 0; i < actions.Count; i++)
        {
            actions[i].InitPassive(this);
        }
        stat = GetComponent<Stats>();
    }
    private void OnDestroy()
    {
        for (int i = 0; i < actions.Count; i++)
        {
            actions[i].CleanupPassive(this);
        }
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            for (int i = 0; i < actions.Count; i++)
            {
                actions[i].Use(this);
            }
        }

    }
}
