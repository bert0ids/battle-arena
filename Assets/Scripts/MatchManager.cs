﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MatchManager : MonoBehaviour
{
    public List<Player> Players = new List<Player>();
    Coroutine mainLoop;

    // Use this for initialization
    void Start()
    {
        Players.ForEach(player => player.ChampionPool.ForEach(champ => champ.actions.ForEach(action => action.SkillUsed.AddListener( delegate { action.isDone = true; } ))));
        mainLoop = StartCoroutine(MatchLoop());
    }

    // Update is called once per frame
    void Update()
    {

    }


    bool allDecided()
    {
        int decidedCount = 0;
        Players.ForEach(player => decidedCount += player.Decided ? 1 : 0);
        return decidedCount >= Players.Count ? true : false;
    }


    void decideMatch()
    {
        for (int i = 0; i < Players.Count; i++)
        {
            if (Players[i].ChampionPool.Count <= 0)
                Players.RemoveAt(i);
        }

        Players.TrimExcess();

        if (Players.Count <= 1)
        {
            StopCoroutine(mainLoop);
            Debug.Log(Players[0].Name + " wins.");
        }
    }

    public void CheckIfDead()
    {
        foreach (Player player in Players)
        {
            if (player.CurrentChampion.ChampStat.HitPoints <= 0)
            {
                player.ChampionPool.RemoveAt(player.ChampionPool.FindIndex(c => c.Equals(player.CurrentChampion)));
                player.CurrentChampion = player.ChampionPool.Count > 0 ? player.ChampionPool[0] : null;
            }
        }
        Players.TrimExcess();
    }

    void setPriority()
    {
        for (int i = 0; i < Players.Count; i++)
        {
            if (i < Players.Count - 1)
            {
                if (Players[i].PickedAction.Priority < Players[i + 1].PickedAction.Priority)
                {
                    Player temp = Players[i];
                    Players[i] = Players[i + 1];
                    Players[i + 1] = temp;
                    i = 0;
                }
                else if (Players[i].PickedAction.Priority == Players[i + 1].PickedAction.Priority)
                {
                    if (Players[i].CurrentChampion.ChampStat.Speed < Players[i + 1].CurrentChampion.ChampStat.Speed)
                    {
                        Player temp = Players[i];
                        Players[i] = Players[i + 1];
                        Players[i + 1] = temp;
                        i = 0;
                    }
                }
            }
        }
    }

    public IEnumerator MatchLoop()
    {
        while (Players.Count > 1)
        {
            foreach (Player p in Players)
            {
                if (p is AIPlayer)
                    StartCoroutine(p.GetComponent<AIPlayer>().PickRandomMove());
            }
            decideMatch();
            yield return new WaitUntil(() => allDecided());
            setPriority();
            Players.ForEach(player => player.PickedAction.Use(player.CurrentChampion.Target));
            foreach (Player player in Players)
            {
                player.PickedAction.Use(player.CurrentChampion.Target);
                yield return new WaitUntil(() => player.PickedAction.isDone);
            }
            Players.ForEach(player => player.Decided = false);
            Debug.Log(Players[0].name);
            CheckIfDead();
            Debug.Log("turn done");
            yield return new WaitForEndOfFrame();
        }
    }
}
