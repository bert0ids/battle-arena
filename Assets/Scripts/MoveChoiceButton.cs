﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveChoiceButton : MonoBehaviour
{
    public Player CurrentPlayer;
    public Actions ActionHolder;
    public List<GameObject> ObjectsToDisable = new List<GameObject>();
    public List<GameObject> ObjectsToEnable = new List<GameObject>();

    public void ApplyMove()
    {
        CurrentPlayer.PickedAction = ActionHolder;
        ObjectsToDisable.ForEach(o => o.SetActive(false));
        ObjectsToEnable.ForEach(o => o.SetActive(true));
        CurrentPlayer.Decided = true;
    }
}
