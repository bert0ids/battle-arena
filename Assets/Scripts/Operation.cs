﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum DamageType
{
    Physical,
    Magic,
    Pure
}

[Serializable]
public class Operation : MonoBehaviour
{
    public string Name;
    public float Speed;
    public Champion Sender;
    public Champion Receiver;
    public DamageType Type;
    public float Damage;

    //public virtual void Use()
    //{
    //    float finalDamage;
    //    finalDamage = Type.Equals(DamageType.Pure) ? Damage :
    //        Type.Equals(DamageType.Normal) ? Damage - Receiver.PhysicalDefense :
    //        Type.Equals(DamageType.Magic) ? Damage - Receiver.MagicDefense : 0;
    //    Receiver.HealthPoints -= Mathf.Clamp(finalDamage, 0, Mathf.Infinity);
    //}
}
