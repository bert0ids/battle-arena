﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public string Name;
    [HideInInspector]
    public bool Decided;
    public List<Champion> ChampionPool = new List<Champion>();
    [HideInInspector]
    public Champion CurrentChampion;
    [HideInInspector]
    public Actions PickedAction;


    // Use this for initialization
    void Start()
    {
        CurrentChampion = ChampionPool.Count > 0 ? ChampionPool[0] : null;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SwapChampion(int index)
    {
        CurrentChampion = ChampionPool[index];
    }
}
