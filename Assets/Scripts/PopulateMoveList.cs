﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopulateMoveList : MonoBehaviour
{
    public Player PlayerOne;
    public GameObject ButtonPrefab;
    public Transform SkillsParent;
    public GameObject MainSelection;

    public void Populate()
    {
        foreach (Transform child in SkillsParent)
        {
            if (!child.GetComponent<UnityEngine.UI.LayoutElement>())
                Destroy(child.gameObject);
        }
        foreach (Actions action in PlayerOne.CurrentChampion.actions)
        {
            MoveChoiceButton temp = Instantiate(ButtonPrefab, SkillsParent).GetComponent<MoveChoiceButton>();
            temp.ActionHolder = action;
            temp.CurrentPlayer = PlayerOne;
            temp.ObjectsToDisable.Add(SkillsParent.gameObject);
            temp.ObjectsToEnable.Add(MainSelection);
            temp.GetComponentInChildren<UnityEngine.UI.Text>().text = action.SkillName;
        }
    }
}
