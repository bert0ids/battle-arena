﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
[System.Serializable]
public class SkillUsedEvent : UnityEvent<Champion> {

}

[CreateAssetMenu(fileName = "Action")]
public class Actions : ScriptableObject {
    public string SkillName;
    public string Description = "to be assigned";
    public int Priority;
    public bool isDone;
    public Passive[] passives;
    public Active[] actives;

    public SkillUsedEvent SkillUsed;
    public void InitPassive(Champion champ)
    {
        for(int i =0;i < passives.Length;i++)
        {
            passives[i].Init(champ);
        }
    }
    public void CleanupPassive(Champion champ)
    {
        for (int i = 0; i < passives.Length; i++)
        {
            passives[i].Cleanup(champ);
        }
    }

    public virtual void Use(Champion champ)
    {
        if (SkillUsed != null)
            SkillUsed.Invoke(champ);

        for(int i =0; i < actives.Length;i++)
        {
            actives[i].Use(champ);
        }
    }
}
