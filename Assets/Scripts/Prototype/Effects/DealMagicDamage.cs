﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Effect_DealMagicDamage")]
public class DealMagicDamage : Active
{
    public float BaseDamage;
    public override void Use(Champion champ)
    {
        champ.Target.ChampStat.TakeDamage(champ, DamageType.Magic,champ.ChampStat.MagicDamage);
      
    }
}
