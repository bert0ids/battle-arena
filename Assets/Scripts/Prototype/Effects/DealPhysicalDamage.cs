﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName ="Effect_DealPhysicalDamage")]
public class DealPhysicalDamage : Active {
    public float BaseDamage;
    public override void Use(Champion champ)
    {
        if (champ.Target == null)
            return;

        champ.Target.ChampStat.TakeDamage(champ, DamageType.Physical, champ.ChampStat.PhysicalDamage);

    }
}
