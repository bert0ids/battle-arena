﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Lifesteal")]
public class LifeSteal : Passive {
    //public float HealPercentage = 100;
    public override void Init(Champion champ)
    {     
        champ.actions[0].SkillUsed.AddListener(HealOnHit);
    }
    public override void Cleanup(Champion champ)
    {
        champ.actions[0].SkillUsed.RemoveListener(HealOnHit);
    }
    public void HealOnHit(Champion champ)
    {
        champ.ChampStat.Heal(champ, champ.ChampStat.PhysicalDamage * champ.ChampStat.LifeStealPercentage / 100);
    }
}
