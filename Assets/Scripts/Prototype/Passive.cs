﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Passive : ScriptableObject
{
    /// <summary>
    /// used for subscribing event
    /// </summary>
    /// <param name="champ"></param>
    public virtual void Init(Champion champ) { }
    /// <summary>
    /// used for unsubscribing event
    /// </summary>
    /// <param name="champ"></param>
    public virtual void Cleanup(Champion champ) { }
}
