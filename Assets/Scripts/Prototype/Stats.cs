﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class TookDamage : UnityEvent<float>
{

}
[System.Serializable]
public class ChampionDeath : UnityEvent<GameObject>
{

}
//[System.Serializable]
//public class ChampionHealed : UnityEvent
//{

//}


public class Stats : MonoBehaviour
{
    #region StatValues
    [Header("Values")]
    public float HitPoints;
    public float MaxHP = 100;

    public float PhysicalDamage = 1;
    public float PhysicalDefense = 1;

    public float MagicDamage = 1;
    public float MagicDefense = 1;

    public float Speed = 1;
    [HideInInspector]
    public float LifeStealPercentage = 0;
    #endregion
    [Header("Events")]
    public TookDamage OnDamageTaken;
    public ChampionDeath OnDeath;
   // public ChampionHealed OnHeal;

    private void Awake()
    {
        HitPoints = MaxHP;
    }

    public void TakeDamage(Champion source, DamageType type, float value)
    {
        switch (type)
        {
            case DamageType.Physical:
                value -= source.ChampStat.PhysicalDefense;
                break;

            case DamageType.Magic:
                value -= source.ChampStat.MagicDefense;
                break;

            default:
                break;
        }

        HitPoints -= value;
        if (OnDamageTaken != null)
            OnDamageTaken.Invoke(value);

        if(HitPoints<= 0)
        {
            HitPoints = 0;
            if (OnDeath != null)
                OnDeath.Invoke(gameObject);
        }
    }

    public void Heal(Champion source, float value)
    {
        HitPoints += value;
        if(HitPoints>= MaxHP)
        {
            HitPoints = MaxHP;
        }
        //if (OnHeal != null)
        //    OnHeal.Invoke(); 
    }
    /// <summary>
    /// this is used in setting values in ui
    /// </summary>
    /// <returns></returns>
    public List<float> ReturnStatValues()
    {
        List<float> values = new List<float>();
        values.Add(MaxHP);
        values.Add(HitPoints);
        values.Add(PhysicalDamage);
        values.Add(PhysicalDefense);
        values.Add(MagicDamage);
        values.Add(MagicDefense);

        return values;

    }
}
